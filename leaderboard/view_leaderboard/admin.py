from django.contrib import admin
from .models import Leaderboard

# Register your models here.

class Display(admin.ModelAdmin):
    list_display = ('display_name','total_score')

admin.site.register(Leaderboard,Display)