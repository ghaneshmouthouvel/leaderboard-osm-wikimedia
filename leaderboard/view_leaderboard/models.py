from django.db import models

# Create your models here.
class Leaderboard(models.Model):
    display_name = models.CharField(max_length=24, primary_key=True)
    user_email = models.EmailField(unique=True)
    osm_username = models.CharField(max_length=256, unique=True, null=True, blank=True)
    osm_orig_score = models.IntegerField(default=0)
    osm_current_score = models.IntegerField(default=0)
    mw_username = models.CharField(max_length=256, unique=True,null=True, blank=True)
    mw_orig_score = models.IntegerField(default=0)
    mw_current_score = models.IntegerField(default=0)
    total_score = models.IntegerField(default=0)
    created_on = models.DateTimeField(auto_now_add=True, auto_now=False)

    class Meta:
        ordering = ['display_name']

    def __str__(self):
        return self.display_name
