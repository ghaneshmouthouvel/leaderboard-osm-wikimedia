from django.shortcuts import render, redirect
from .forms import get_data
import asyncio
from .osm_api_wrapper import osm_get_score
from .mw_api_wrapper import mw_get_score
from view_leaderboard.models import Leaderboard
# Create your views here.

def index(request):
    form = get_data
    error_message = ""
    osm_score = 0
    mw_score = 0
    if request.method == 'POST':
        form = get_data(request.POST)
        if form.is_valid():
            display_name = form.cleaned_data['display_name'].strip()
            osm_username = form.cleaned_data['osm_username'].strip()
            mw_username = form.cleaned_data['mw_username'].strip()
            email_address = form.cleaned_data['email'].strip()
            # Verifying USER (Open Street Map)
            if osm_username.strip() != '':
                try:
                    osm_score = asyncio.run(osm_get_score(osm_username))
                except Exception:
                    error_message = 'Invalid OSM Username'
                    return render(request, 'mainpage/index.html', {'form': form, 'error': error_message})
            # Verifying USER (MediaWiki)
            if mw_username.strip() != '':
                try:
                    mw_score = asyncio.run(mw_get_score(mw_username))
                except Exception:
                    error_message += ' Invalid MediaWiki Username'
                    return render(request, 'mainpage/index.html', {'form': form, 'error': error_message})
            # Saving the Data in Database
            userdb = Leaderboard()
            userdb.display_name = display_name
            userdb.user_email = email_address
            total = osm_score + mw_score
            userdb.total_score = total
            try:
                userdb.save()
                return redirect('view_leaderboard:leaderboard')
            except:
                error_message = "USER ALREADY EXITS IN DATABASE"
    return render(request, 'mainpage/index.html', {'form': form, 'error': error_message})
